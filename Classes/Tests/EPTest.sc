EPTest1 : UnitTest {
	test_check_classname {
		var result = EP.new;
		this.assert(result.class == EP);
	}
}


EPTester {
	*new {
		^super.new.init();
	}

	init {
		EPTest1.run;
	}
}
